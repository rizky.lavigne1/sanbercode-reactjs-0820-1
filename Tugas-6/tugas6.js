//Soal 1

let v = 3.14;

var luasLingkaran = (r) => {
    return v * (r*r);
}

console.log(luasLingkaran(2));
console.log('----------------------------------------------');
console.log();

//Soal 2

const V = 3.14 * 2;

var kelLingkaran = (r) => {
    return V * r;
}
console.log(kelLingkaran(3));
console.log('----------------------------------------------');
console.log();

//Soal 3

let kalimat = "";

const tambahKata = (kata) => {
    return kalimat += `${kata} `;
}

tambahKata("saya");
tambahKata("adalah");
tambahKata("seorang");
tambahKata("fontend");
tambahKata("developer");

console.log(kalimat);

console.log('----------------------------------------------');
console.log();

//Soal 4

const newFunction = (firstName, lastName) =>{
    return {
        firstName,
        lastName,
        fullName: () => {
           console.log(`${firstName} ${lastName}`);
        }
    }
}
   
newFunction("William", "Imoh").fullName();

console.log('----------------------------------------------');
console.log();

//Soal 5

const newObject = {
    firstName: "Harry",
    lastName: "Potter",
    destination: "Horwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation);

console.log('----------------------------------------------');
console.log();


const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];

let gabungArr = [...west, ...east];

console.log(gabungArr);

console.log('----------------------------------------------');
console.log();