//Soal 1 Release 0

class Animal {
    constructor(name, leg){
        this._name = name;
        this._legs = leg;
        
    }
    get name(){
        return this._name;
    }
    set name(x){
        this._name = x;
    }
    get cold_blooded(){
        return false;
    }
    get legs(){
        return this._leg = 4;
       
        
    }
   
}
sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

//Soal 1 Release 2

class Ape extends Animal {
    constructor(name,yell){
        super(name);
        this._yell = yell;
    }
    yell (){
        return "auoooo";
    }
    
}
var sunggokong = new Ape("kera sakti");

console.log(sunggokong.name);
console.log(sunggokong.yell());

class Frog extends Animal {
    constructor(name,jump){
        super(name);
        this._jump = jump;
    }
    jump(){
        return "hop hop";
    }
}

let kodok = new Frog("Buduk");

console.log(kodok.name);
console.log(kodok.jump());


// Soal 2

class Clock {
    constructor({template}){
        this.template = template;

    }

    render(){
        let date = new Date();

        let  hours = date.getHours();
        if(hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if(mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if(secs < 10) secs = '0' + secs;

        let output =  this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop(){
        clearInterval(this.timer);      
    }

    start(){
        this.render()
        this.timer = setInterval(() => this.render(), 1000);


    }

}

var clock = new Clock({template: 'h:m:s'});
clock.start();
clock.stop(); 