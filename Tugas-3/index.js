// Soal 1

var kataPertama = "saya";
var katakedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama + " " + (katakedua.charAt(0).toUpperCase())+(katakedua.substring(1)) + " " + kataKetiga + " " +kataKeempat.toUpperCase());

// Soal 2

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var kata1 = Number(kataPertama);
var kata2 = Number(kataKedua);
var kata3 = Number(kataKetiga);
var kata4 = Number(kataKeempat);

var jumlah = kata1+kata2+kata3+kata4

console.log(jumlah);

// Soal 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 13);
var kataKetiga = kalimat.substr(15, 3);
var kataKeempat = kalimat.substr(19, 5);
var kataKelima = kalimat.substr(25, 6)


console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

// Soal 4

var nilai = 75;

if (nilai >= 80) {
    console.log('indeksnya A');
} else if (nilai >= 70 && nilai < 80){
    console.log('indeksnya B');
} else if (nilai >= 60 && nilai < 70){
    console.log('indeksnya C');
} else if (nilai >= 50 && nilai < 60){
    console.log('indeksnya D');
} else if (nilai < 50){
    console.log('indeksnya E');
}

// Sola 5

var tanggal = 24;
var bulan = 8;
var tahun = 1995;

switch(bulan){
    case 1 :
        console.log(tanggal + " Januari " + tahun);
        break;
    case 2 :
        console.log(tanggal + " Februari " + tahun);
        break;
    case 3 :
        console.log(tanggal + " Maret " + tahun);
        break;
    case 4 :
        console.log(tanggal + " April " + tahun);
        break;
    case 5 :
        console.log(tanggal + " Mei " + tahun);
        break;
    case 6 :
        console.log(tanggal + " Juni " + tahun);
        break;
    case 7 :
        console.log(tanggal + " Juli " + tahun);
        break;
    case 8 :
        console.log(tanggal + " Agustus " + tahun);
        break;
    case 9 :
        console.log(tanggal + " September " + tahun);
        break;
    case 10 :
        console.log(tanggal + " Oktober " + tahun);
        break;
    case 11 :
        console.log(tanggal + " November " + tahun);
        break;
    case 12 :
        console.log(tanggal + " Desember " + tahun);
        break;        
    default :
        console.log("Data tidak ada");
            break;
}

