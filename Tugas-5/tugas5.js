// soal 1

function Hallo(){
    console.log("Halo Sanbers!");
}

Hallo();


console.log('-------------------------------------------------');
console.log();


// soal 2

function kalikan(){
	return num1 * num2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log('-------------------------------------------------');
console.log();

// soal 3
function introduce(){
    
    return "Nama saya " + name + ", umur saya " + age + " tahun, " + "alamat saya di " + (address.charAt(0)).toLowerCase() + address.substr(1)+ ", dan saya punya hobby yaitu " + hobby;   
}

var name = "John";
    var age = 30;
    var address = "Jalan belum jadi";
    var hobby = "Gaming!";


var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

console.log('-------------------------------------------------');
console.log();

// soal 4

var arrayDataPeserta = ["Asep", "laki-laki", "baca buku", 1992];

var obj = {};

obj.nama = arrayDataPeserta[0];
obj.jenis_kelamin = arrayDataPeserta[1];
obj.hobby = arrayDataPeserta[2];
obj.tahun_lahir = arrayDataPeserta[3];
console.log(obj);

console.log('-------------------------------------------------');
console.log();


// soal 5

var buah = [{no: 1, nama: "strawbery", warna: "merah", adaBijinya: false, harga: 9000}, {no: 2, nama: "jeruk", warna: "oranye", adaBijinya: true, harga: 8000}];

console.log(buah[0]);

console.log('-------------------------------------------------');
console.log();

// soal 6

var dataFilm = [];

function tambahFilm(nama, durasi, genre, tahun){
    
    var obj = {};
    
    obj.nama = nama,
    obj.durasi = durasi,
    obj.genre = genre,
    obj.tahun = tahun,

    dataFilm.push(obj);
}
tambahFilm("Fastfive", "2 jam", "Action", 2015);
tambahFilm("Jurasic World", " 2 jam", "Action", 2017);

console.log(dataFilm);

console.log('-------------------------------------------------');
console.log();
