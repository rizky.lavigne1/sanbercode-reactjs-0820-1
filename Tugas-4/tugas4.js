

// soal 1
var loopPertama = 2;
console.log('LOOPING PERTAMA');
while(loopPertama <= 20) {
    console.log(loopPertama + ' - I love coding');
loopPertama += 2;
}

var loopKedua = 20;
console.log('LOOPING KEDUA');
while(loopKedua >= 2){
    console.log(loopKedua + ' - I will become a frontend developer');
loopKedua -= 2;
} 

console.log('-------------------------------------------------');
console.log();

// soal 2

var a = 'Santai';
var b = 'Berkualitas';
var c = 'I Love Coding';

for(i = 1; i <= 20; i++){
    if(i % 2 == 1){
        if(i % 3 == 0 && i % 2 == 1){
            console.log(i + ' - ' + c);
        }else{
        console.log(i + ' - ' + a);}
    } else if(i % 2 == 0){
        console.log(i + ' - ' + b);
    }
}

console.log('-------------------------------------------------');
console.log();


//soal 3

var s = '';
for(i = 0; i < 7; i++){
    s += '#';
    console.log(s); 
   
}

console.log('-------------------------------------------------');
console.log();

// soal 4

var kalimat = "saya sangat senang belajar javascript";

console.log(kalimat.split(" "));

console.log('-------------------------------------------------');
console.log();


// soal 5

var daftarBuah = ["2.Apel", "5.Jeruk", "3.Anggur", "4.Semangka", "1.Mangga"];
var buah = daftarBuah.sort();

for(i = 0; i < buah.length; i++){
    console.log(buah[i]);
}

console.log('-------------------------------------------------');
console.log();